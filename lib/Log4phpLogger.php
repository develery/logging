<?php


namespace Develery\Components\Logging;

use Psr\Log\InvalidArgumentException;
use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

/**
 * Class Log4phpLogger
 * @package Develery\Components\Logging
 *
 * PSR-3 LoggerInterface-t implementalo log4php logger
 *
 *
 * https://www.php-fig.org/psr/psr-3/
 * https://logging.apache.org/log4php/
 * https://logging.apache.org/log4php/apidocs/class-LoggerMDC.html
 *
 */
class Log4phpLogger extends AbstractLogger
{

    const EXCEPTION_KEY = 'exception';

    /** @var \Logger log4php logger instance */
    private $logger;

    /** @param string $loggerName */
    public function __construct($loggerName = 'main')
    {
        $this->logger = \Logger::getLogger((string)$loggerName);
    }


    /**
     * @param string $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        $message = (string)$message;

        /** @var \Exception */
        $exceptionInfo = null;

        // $context-ben van exception?
        if (array_key_exists(self::EXCEPTION_KEY, $context) && $context[self::EXCEPTION_KEY] instanceof \Exception ) {
            $exceptionInfo = $context[self::EXCEPTION_KEY];
        }

        try {
            $this->initMDC($context);

            $message = $this->interpolate($message, $context);

            switch ($level) {
                case LogLevel::EMERGENCY:
                case LogLevel::ALERT:
                case LogLevel::CRITICAL:
                    $this->logger->fatal($message, $exceptionInfo);
                    break;
                case LogLevel::ERROR:
                    $this->logger->error($message, $exceptionInfo);
                    break;
                case LogLevel::WARNING:
                case LogLevel::NOTICE:
                    $this->logger->warn($message, $exceptionInfo);
                    break;
                case LogLevel::INFO:
                    $this->logger->info($message, $exceptionInfo);
                    break;
                case LogLevel::DEBUG:
                    $this->logger->debug($message, $exceptionInfo);
                    break;
                default:
                    throw new InvalidArgumentException("Unknown log level: " . $level);
            }
        } finally {
            $this->cleanMDC($context);
        }

    }

    /**
     * @param string	$message
     * @param array		$context
     *
     * @return string
     */
    private function interpolate($message, array $context = array())
    {
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $this->getStringValue($val);
        }

        return strtr($message, $replace);
    }

    /**
     * @param mixed $value  Convertable value (int, float, DateTime, string, array, object)
     *
     * @return string
     */
    private function getStringValue($value)
    {
        $stringValue = '';

        if($value === null) {
            $stringValue = 'NULL';
        } else {
            if(is_object($value) && method_exists($value, '__toString')) {
                $stringValue = strval($value);
            } else {
                if(is_array($value)) {
                    $toImplode = array();
                    foreach ($value as $key => $val) {
                        $toImplode[] = var_export($key, true) . ' => ' . $this->getStringValue($val);
                    }
                    $stringValue = '[' . implode(', ', $toImplode) . ']';
                } else {
                    if($value instanceof \DateTime) {
                        $stringValue = $value->format('Y-m-d H:i:s');
                    } else {
                        $stringValue = var_export($value, true);
                    }
                }
            }
        }

        return $stringValue;
    }

    /**
     * @param array $context
     */
    private function initMDC(array $context = array())
    {
        foreach ($context as $key => $val) {
            \LoggerMDC::put($key, $val);
        }
    }

    /**
     * @param array $context
     */
    private function cleanMDC(array $context = array())
    {
        foreach ($context as $key => $val) {
            \LoggerMDC::remove($key);
        }
    }

}