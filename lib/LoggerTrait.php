<?php


namespace Develery\Components\Logging;


use Develery\Components\Logging\Log4phpLogger;
use Psr\Log\LoggerInterface;

trait LoggerTrait
{

    /** @var Log4phpLogger[] */
    protected static $psrLoggers = array();

    /**
     * @param string $customLoggerName
     *
     * @return Log4phpLogger
     */
    final protected static function logger(string $customLoggerName = null)
    {
        $loggerName = str_replace('\\', '.', ( ( $customLoggerName === null || strlen($customLoggerName) === 0 ) ? get_called_class() : $customLoggerName ));

        if ( !isset(self::$psrLoggers[$loggerName]) || self::$psrLoggers[$loggerName] === null ) {
            self::$psrLoggers[$loggerName] = new Log4phpLogger($loggerName);
            self::$psrLoggers[$loggerName]->debug(sprintf("'%s' logger created.", $loggerName));
        }

        return self::$psrLoggers[$loggerName];
    }

}